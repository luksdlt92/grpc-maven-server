package com.gitlab.luksdlt92.grpc.father.services;

import io.datanerd.generated.helloworld.GreeterGrpc;
import io.datanerd.generated.helloworld.HelloReply;
import io.datanerd.generated.helloworld.HelloRequest;
import io.grpc.stub.StreamObserver;

public class SomeClass extends GreeterGrpc.GreeterImplBase {

    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        HelloReply reply = HelloReply.newBuilder().setMessage("Hello folk!").build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
